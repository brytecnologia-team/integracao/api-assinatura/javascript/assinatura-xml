module.exports = {

    //Request identifier
    NONCE: 1,

    // Available values: 'true' = Original document in any format and 'false' = Original document in xml format.
    BINARY_CONTENT: 'false',

    // Available values: "BASIC", "CHAIN", "CHAIN_CRL", "TIMESTAMP", "COMPLETE", "ADRB", "ADRT", "ADRV", "ADRC", "ADRA", "ETSI_B", "ETSI_T", "ETSI_LT" and "ETSI_LTA".
    PROFILE: 'BASIC',

    // Available values: ENVELOPED = signature is contained in the content of the document, ENVELOPING = signature contains the content of the document and DETACHED = signature and document content are separate
    SIGNATURE_FORMAT: 'ENVELOPED',

    // Available values: "SHA1", "SHA256" e "SHA512".
    HASH_ALGORITHM: 'SHA256',

    // Available values: "SIGNATURE", "CO_SIGNATURE	" and "COUNTER_SIGNATURE".
    OPERATION_TYPE: 'SIGNATURE',

    // Identifier of the original document within a batch
    NONCE_OF_ORIGINAL_DOCUMENT: 1,

    //location where the original document is stored    
    ORIGINAL_DOCUMENT_PATH: './arquivo-assinar/documento.xml'

}